import TOTAL_RECALL_ITEMS from '@salesforce/resourceUrl/totalRecallItems';

const itemsPool = [
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/airplane-flight.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/air_balloon.jpeg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/apple.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/avocado.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/ball.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/bananas.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/bangers.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/bbq.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/bear.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/bike.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/binocular.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/books.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/bridge.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/broccoli.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/bunkbed.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/burger.jpeg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/bus.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/cake.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/calculator.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/camel.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/candles.png'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/capsicum-green.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/capsicum-red.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/capsicum-yellow.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/car.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/carrot.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/castle.jpeg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/cat.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/cauliflower.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/celery.png'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/chair.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/cheese.png'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/cherrytree.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/chicken.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/chickewings.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/child.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/chokolate.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/christmastree.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/codey.png'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/colosseum.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/coronabeer.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/cows.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/croc.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/cucumber.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/cupcake.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/diamond.png'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/dolphin.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/donaldduck.jpeg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/donkeys.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/donuts.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/echidna.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/eggs.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/fairy.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/ferry.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/fishingnet.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/flamingo.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/flower.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/fork.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/french-fries.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/frog.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/giraffe.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/glasses.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/globe.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/goofy.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/grapes.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/hat.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/headset.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/heartvegetables.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/helmet.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/hippo.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/honey.png'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/horse.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/icecream.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/kangaroo.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/lamp.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/lemons.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/lightning.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/lollipop.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/magichat.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/magnifyglass.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/maki-rolls.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/matterhorn.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/meatballs.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/mermaid.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/milk.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/milo.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/moon.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/mower.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/newspaper.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/olives.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/orchid.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/palm.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/pancakes.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/paris-eiffel-tower.png'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/parrot.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/pasta.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/pear.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/peas.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/phoneold.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/pipe.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/pizza.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/polarbear.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/pomegranate.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/popcorn.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/raspberry.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/rat.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/ring.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/rockinghorse.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/rollerskate.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/rottweiler.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/salmondish.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/samosa.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/scales.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/scissor.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/screw.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/shark.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/shoe.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/shrimps.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/smoothie.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/snake.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/star.png'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/steak.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/strawberry.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/suitcases.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/sunflowers.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/sushi-plate.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/swimmingpool.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/swing.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/taco.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/taxi.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/tennisracket.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/tent.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/tiger.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/tikkamasala.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/toilet.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/tomato.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/tooth.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/toothbrush.png'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/tractor.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/trafficlight.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/train.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/trampoline.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/trophy.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/truck.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/umbrella.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/watch.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/watermelon.png'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/wombat.jpg'
    },
    {
        imageUrl: TOTAL_RECALL_ITEMS + '/totalRecallItems/zebra.jpg'
    }
]

export { itemsPool };