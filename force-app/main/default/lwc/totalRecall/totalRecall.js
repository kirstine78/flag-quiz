import { LightningElement, track } from 'lwc';
import { itemsPool } from './itemsPool';
import FORM_FACTOR from '@salesforce/client/formFactor';
// import your images from static resources
import RESULT_ICONS from '@salesforce/resourceUrl/resulticons';

const TIMER_MILLISECONDS = 3500;  // Play mode
// const TIMER_MILLISECONDS = 10;  // Debugging mode
const ITEMS_PER_GAME = 3;  // this is overwritten when user clicks buttons

export default class TotalRecall extends LightningElement {
    greenTick = RESULT_ICONS + '/resulticons/greenticktransparent.png';
    redCross = RESULT_ICONS + '/resulticons/redcrosstransparent.png';

    itemsPerGame = ITEMS_PER_GAME;
    itemsPool = itemsPool;
    itemsList = [];  // hold ITEMS_PER_GAME item objects from itemsPool in random order
    itemUrlsSortedByUserList;
    correctOrderAndUserSortedOrderList = [];
    currentItem;
    counter = 0;
    // timerInterval;
    timerTimeout;
    displayReadyPlay = false;
    showSolveOrder = false;
    hasGameStarted = false;
    showResult = false;

    /**
     * We want to clear the time interval, if for example
     * the Total Recall Tab is pressed during a game.
     */
    disconnectedCallback() {
        // console.log('disconnectedCallback');
        window.clearInterval(this.timerTimeout);
    }

    /**
     * When one of the Play buttons from Menu page is clicked,
     * we get the number we want to set itemsPerGame to.
     * Then startGameHandler is called.
     */
    buttonOptionHandler(event) {
        // console.log('buttonOptionHandler');

        this.itemsPerGame = Number(event.target.dataset.number);
        // console.log('this.itemsPerGame:', this.itemsPerGame);

        this.startGameHandler();
    }

    /**
     * Update flag. 
     * Call multiple methods required to start the game.
     */
    startGameHandler() {
        // console.log('startGameHandler');

        // hide Play button
        this.hasGameStarted = true;

        this.buildItemsList();
        this.displayGetReady();
        // console.log('about to call startInterval');
        // this.startInterval();
        // console.log('after call to startInterval');
        
        // console.log('about to call repeatTimer');
        this.repeatTimer(this.itemsPerGame + 1);
        // console.log('after call to repeatTimer');
    }

    /**
     * Get random set of numbers that are used to decide which objects
     * from the itemsPool to be used for a particular game.
     * These randomly chosen objects are pushed to the itemsList. 
     */
    buildItemsList() {
        // itemsList has to consist of ITEMS_PER_GAME item objects
        // We have to choose randomly from the itemsPool
        let randomUniqueNumbersSet = this.getSetOfRandomNumbers(this.itemsPerGame, this.itemsPool.length - 1);
        for (let i of randomUniqueNumbersSet) {
            // console.log(i);

            // hack, first convert to JSON string, then convert back to JavaScript object
            this.itemsList.push(JSON.parse(JSON.stringify(this.itemsPool[i])));
        }
        // console.log('this.itemsList:', this.itemsList);
    }

    /**
     * Set the displayReadyPlay flag to true, so 
     * the text indicating to user that they are to get ready
     * shows.
     */
    displayGetReady() {
        this.displayReadyPlay = true;
    }

    /**
     * Set the displayReadyPlay flag to false, so 
     * the text indicating to user that they are to get ready
     * is hidden.
     */
    hideGetReady() {
        this.displayReadyPlay = false;
    }

    /**
     * Set the currentItem to be an object from the itemsList
     * based on where the counter is up to.
     */
    setCurrentItem() {
        // console.log('setCurrentItem');
        // console.log('this.counter:', this.counter);
        this.currentItem = this.itemsList[this.counter];
        // console.log('this.currentItem.name:', this.currentItem.name);
    }

    /**
     * Repeatedly call method that Sets a timeout.
     * Number of repeats are driven by parameter numberOfRepeats,
     * which will be one more than length of list of images to show. 
     * This is to ensure that the last image in the list will also be shown
     * the specific milliseconds duration.
     */
    async repeatTimer(numberOfRepeats) {
        // console.log('in async repeatTimer');
        for (let count = 0; count < numberOfRepeats; count++) {
            await this.startTimer();
            window.clearInterval(this.timerTimeout);
            this.hideGetReady();
            // console.log('this is count:', count);
            
            // we want to keep going for one more round than there are items in list
            if (count < this.itemsList.length) {
                // console.log('we set an item');
                this.setCurrentItem();
                // keep going
                this.updateCounter();
                // console.log('going counter:', this.counter);
            } else {
                // console.log('we set item to null');
                this.currentItem = null;

                // console.log('we want to stop now');
                // last item has been shown for ok amount of time, so stop it now
                // window.clearInterval(this.timerTimeout);
                this.showSolveOrder = true;
                // console.log('this.showSolveOrder:', this.showSolveOrder);
                // console.log('stopped:', this.counter);
            }
        }
        // console.log('end of async repeatTimer');
    }

    /**
     * Set a timeout
     */
    startTimer() {
        return new Promise(resolve => {
            this.timerTimeout = setTimeout(() => {
                // console.log('setTimeout');
                resolve();
            }, TIMER_MILLISECONDS);
        });
    }

    /**
     * This drives the interval between images to be shown. 
     * For each interval we show a new image. 
     * This method gave issues. Sometimes an image was so delayed in showing, 
     * that the next image was shown before the previous image had a chance to 
     * be shown to user.
     */
    // startInterval() {
    //     // good to use window.setInterval instead of just setInterval because older browsers don't always understand just setInterval
    //     // also make sure that we deal with a number because parent might pass a string like "4000"
    //     let countIntervals = 0;
    //     // console.log('startInterval');

    //     this.timerInterval = window.setInterval(() => {
    //         // every x seconds call slideSelectionHandler method

    //         console.log('countIntervals:', countIntervals);

    //         this.hideGetReady();

    //         // we want to keep going for one more round than there are items in list
    //         if (countIntervals < this.itemsList.length) {
    //             // console.log('we set an item');
    //             this.setCurrentItem();
    //             // keep going
    //             this.updateCounter();
    //             // console.log('going counter:', this.counter);
    //         } else {
    //             // console.log('we set item to null');
    //             this.currentItem = null;

    //             // console.log('we want to stop now');
    //             // last item has been shown for ok amount of time, so stop it now
    //             window.clearInterval(this.timerInterval);
    //             this.showSolveOrder = true;
    //             // console.log('this.showSolveOrder:', this.showSolveOrder);
    //             // console.log('stopped:', this.counter);
    //         }
    //         countIntervals++;
    //     }, TIMER_MILLISECONDS);
    //     console.log('bottom of startInterval');
    // }

    /**
     * Updates the counter that keeps track of the current item in the game. 
     */
    updateCounter() {
        // console.log('before update counter:', this.counter);
        if (this.itemsList.length - 1 === this.counter) {
            this.counter = null;
        } else {
            this.counter++;
        }
        // console.log('after update counter:', this.counter);
    }

    /**
     * Get a set of unique numbers where length of set will be sizeOfSet.
     * Numbers can range from 0 -> max, both included
     */
    getSetOfRandomNumbers(sizeOfSet, max) {
        // console.log('getSetOfRandomNumbers');
        // console.log('sizeOfSet', sizeOfSet);
        // console.log('max', max);
        const numberSet = new Set()
        while (numberSet.size < sizeOfSet) {
            numberSet.add(Math.floor(Math.random() * (max + 1)));
        }
        // console.log('numberSet', numberSet);
        return numberSet;
    }

    /**
     * Event handler for when user submits the order of images
     * he believes is right. 
     * We get the array of player's ordered images. 
     * We update certain flags that drives which section of page is shown. 
     * Then we call populateCorrectOrderAndUserSortedOrderList.
     */
    handleOnSubmitOrder(event) {
        // console.log('handleOnSubmitOrder');
        // console.log('event.detail:', event.detail);

        // get the event detail that holds the array of item img url's sorted by user
        this.itemUrlsSortedByUserList = event.detail;
        this.showSolveOrder = false;
        this.showResult = true;
        this.populateCorrectOrderAndUserSortedOrderList();
    }

    /**
     * We populate the list correctOrderAndUserSortedOrderList. 
     * It holds objects that combines the correct order image urls and
     * the player's image url's and a result icon (red cross or green tick).
     */
    populateCorrectOrderAndUserSortedOrderList() {
        // console.log('populateCorrectOrderAndUserSortedOrderList');
        for (let counter = 0; counter < this.itemsPerGame; counter++) {
            let comboObj = {};
            comboObj.correctItemUrl = this.itemsList[counter].imageUrl;
            comboObj.userOrderedItemUrl = this.itemUrlsSortedByUserList[counter];
            comboObj.resultIcon = '';
            if (comboObj.correctItemUrl === comboObj.userOrderedItemUrl) {
                comboObj.resultIcon = this.greenTick;
            } else {
                comboObj.resultIcon = this.redCross;
            }
            this.correctOrderAndUserSortedOrderList.push(comboObj);
        }
    }

    /**
     * Handler for when user clicks the Play Again button. 
     * The reset method is called, and then the startGameHandler method. 
     */
    playAgainHandler() {
        this.reset();
        this.startGameHandler();
    }

    /**
     * This method resets variables used to keep track of 
     * a particular game. 
     */
    reset() {
        this.itemsList = [];
        this.itemUrlsSortedByUserList = [];
        this.correctOrderAndUserSortedOrderList = [];
        this.counter = 0;
        this.displayReadyPlay = false;
        this.showSolveOrder = false;
        this.hasGameStarted = false;
        this.showResult = false;
    }

    // get the form factor
    get isDesktop() {
        switch(FORM_FACTOR) {
            case 'Large':
                return true;
            case 'Medium':
                return false;
            case 'Small':
                return false;
            default:
        }
    }
}