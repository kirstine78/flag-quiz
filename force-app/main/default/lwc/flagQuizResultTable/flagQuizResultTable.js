/**
 * Author: Kirstine Broerup Nielsen
 */

import { LightningElement, api } from 'lwc';
// import your images from static resources
import RESULT_ICONS from '@salesforce/resourceUrl/resulticons';

export default class FlagQuizResultTable extends LightningElement {
    greenTick = RESULT_ICONS + '/resulticons/greenticktransparent.png';
    redCross = RESULT_ICONS + '/resulticons/redcrosstransparent.png';

    @api
    isCountryMode;

    @api
    resultArray;
}