import { LightningElement, track } from 'lwc';
import FORM_FACTOR from '@salesforce/client/formFactor';
// this example: animate is a zip folder
import ANIMATE from '@salesforce/resourceUrl/animate';
// we need a special method that loads that particular library into our component
// import methods from the platformResourceLoader module
import { loadStyle} from 'lightning/platformResourceLoader';

// import { initializeEmptyBoard, setInitialFourPiecesSetup, isPositionOccupied, getCoordinates, 
//     isCheckDirectionUpNeeded, isCheckDirectionUpRightNeeded, isCheckDirectionRightNeeded, 
//     isCheckDirectionDownRightNeeded, isCheckDirectionDownNeeded, isCheckDirectionDownLeftNeeded,
//     isCheckDirectionLeftNeeded, isCheckDirectionUpLeftNeeded } from './utils';

// import your images from static resources
import OTHELLO_PIECES from '@salesforce/resourceUrl/othelloPieces';

const BLACK = 'Black';
const WHITE = 'White';

// Always a square meaning X length and Y length are the same
const SQUARE_LENGTH = 8;
const Y_AXIS_LENGTH = SQUARE_LENGTH;
const X_AXIS_LENGTH = SQUARE_LENGTH;

export default class Othello extends LightningElement {
    computerDelayMove = 3500;
    removeEffectsDelay = 1000;

    @track
    board;

    blackPieceUrl = OTHELLO_PIECES + '/othelloPieces/black.png';
    whitePieceUrl = OTHELLO_PIECES + '/othelloPieces/white.png';

    playerComputer = {pieceUrl: null, color: null, canMove: true, piecesCount: 0};
    playerHuman = {pieceUrl: null, color: null, canMove: true, piecesCount: 0};

    currentCoordinatesWherePiecesToFlip = [];

    noPossibleMoveMsg = 'No possible move, turn goes back to';
    winnerMsg = '';

    isItHumanPlayersTurn;
    hasGameStarted = false;
    isAllMovesExhaustedForBoth = false;

    // renderedCallback is called again and again if component changes
    isLibraryLoaded = false;

    // we have to load the script
    // we only want to load script when the html has been completely rendered, NOT before
    renderedCallback() {

        if (this.isLibraryLoaded) {
            return;
        } else {
        
            /**
             * Using third-party CSS library exercise.
             */
            loadStyle(this, ANIMATE + '/animate/animate.min.css').then(() => {
                // loaded successfully
            }).catch(error => {
                console.log('we got an error:', error);
            });
            this.isLibraryLoaded = true;
        }
    }

    playAgainHandler() {
        this.board = null;
        this.isAllMovesExhaustedForBoth = false;
        
        this.playerComputer.pieceUrl = null;
        this.playerComputer.color = null;
        this.playerComputer.canMove = true;
        this.playerComputer.piecesCount = 0;
        
        this.playerHuman.pieceUrl = null;
        this.playerHuman.color = null;
        this.playerHuman.canMove = true;
        this.playerHuman.piecesCount = 0;

        this.currentCoordinatesWherePiecesToFlip = [];

        this.winnerMsg = '';
    
        this.isItHumanPlayersTurn = null;
        this.hasGameStarted = false;
        this.isAllMovesExhaustedForBoth = false;

        this.startGameHandler();
    }

    startGameHandler() {
        console.log('startGameHandler');

        this.hasGameStarted = true;
        // randomize who goes first; human or computer
        this.setPlayersTurn();
        this.initializeEmptyBoard();
        this.setInitialFourPiecesSetup();
        this.setCountForPlayers();

        // Check if it is computer that goes first
        if (!this.isItHumanPlayersTurn) {
            this.randomMove();
        }
    }


    setPlayersTurn() {
        // BLACK always goes first
        // we want random 0 or 1
        let maxWhichIsExcluded = 2;

        if (Math.floor(Math.random() * maxWhichIsExcluded)) {
            // 1 returned
            console.log('one returned let human go first');
            this.isItHumanPlayersTurn = true;
            this.playerHuman.pieceUrl = this.blackPieceUrl;
            this.playerHuman.color = BLACK;
            this.playerComputer.pieceUrl = this.whitePieceUrl;
            this.playerComputer.color = WHITE;
        } else {
            // 0 returned
            console.log('zero returned let computer go first');
            this.isItHumanPlayersTurn = false;
            this.playerHuman.pieceUrl = this.whitePieceUrl;
            this.playerHuman.color = WHITE;
            this.playerComputer.pieceUrl = this.blackPieceUrl;
            this.playerComputer.color = BLACK;
        }
    }

    /**
     * Handler for clicking on board to position a piece. 
     * Will only ever be called by human click. 
     * @param {*} event 
     */
    onPositionClickHandler(event) {
        console.log('onPositionClickHandler');
        console.log('this.isItHumanPlayersTurn:', this.isItHumanPlayersTurn);
        // console.log('this.playerHuman.color:', this.playerHuman.color);
        // console.log('this.playerComputer.color:', this.playerComputer.color);

        // We only want action to happen if it is the Human's turn to move 
        if (this.isItHumanPlayersTurn) {
            let positionId;
        
            // Check if image was clicked in case piece is on position
            if (event.target.tagName === 'IMG') {
                // console.log('IMG clicked');
                let imgElement = event.target;
                positionId = Number(imgElement.parentElement.getAttribute('data-id'));
                // let divId = imgElement[0].parentNode.getAttribute('data-id');
                // console.log('positionId:', positionId);
            } else {
                // console.log('div clicked');
                /* We do mathematical operation so we want to confirm that event.target.dataset.id 
                truly is a number not a string, because the DOM most often often returns it as a string */
                positionId = Number(event.target.dataset.id);
                // console.log('positionId:', positionId);
            }
            this.handlePositioning(positionId);
        } else {
            // Not human's turn to move so we ignore the click
            console.log('Not human\'s turn to move so we ignore the click');
        }
    }

    handlePositioning(positionId) {
        console.log('handlePositioning');
        console.log('positionId:', positionId);        

        // make sure that we wipe this.currentCoordinatesWherePiecesToFlip
        this.currentCoordinatesWherePiecesToFlip = [];

        let xyCoordinate = this.getCoordinateFromPositionId(positionId);
        let isPositionLegal = false;
        if (this.isItHumanPlayersTurn) {
            isPositionLegal = this.isPositionLegal(xyCoordinate, this.playerHuman);
        } else {
            isPositionLegal = this.isPositionLegal(xyCoordinate, this.playerComputer);
        }
        console.log('isPositionLegal:', isPositionLegal);

        if (isPositionLegal) {
            // we will place piece on position of the current player's color
            if (this.isItHumanPlayersTurn) {
                this.board[xyCoordinate.y][xyCoordinate.x].imgUrl = this.playerHuman.pieceUrl;
                this.board[xyCoordinate.y][xyCoordinate.x].color = this.playerHuman.color;
            } else {
                this.board[xyCoordinate.y][xyCoordinate.x].imgUrl = this.playerComputer.pieceUrl;
                this.board[xyCoordinate.y][xyCoordinate.x].color = this.playerComputer.color;
            }

            // this.printBoard();

            // we need to flip the flanked pieces
            this.flipPieces();

            // do we need to remove class again?
            this.removeEffects();
            console.log('after call to removeEffects');

            // update count for each player
            this.setCountForPlayers();

            // then update flag to the opposite player
            this.isItHumanPlayersTurn = this.isItHumanPlayersTurn ? false : true;
            console.log('this.isItHumanPlayersTurn after flipping:', this.isItHumanPlayersTurn);
            console.log('check if he can move');            

            // check if the player can actually make a move
            if(this.canPlayerMakeMove()) {
                // yes the player can make legal move
                console.log('this.isItHumanPlayersTurn after flipping:', this.isItHumanPlayersTurn);
                console.log('yes can move');
                if (this.isItHumanPlayersTurn) {
                    this.playerHuman.canMove = true;
                } else {
                    this.playerComputer.canMove = true;
                }
            } else {
                // no the player cannot make legal move
                console.log('this.isItHumanPlayersTurn after flipping:', this.isItHumanPlayersTurn);
                console.log('no cannot move');
                if (this.isItHumanPlayersTurn) {
                    this.playerHuman.canMove = false;
                } else {
                    this.playerComputer.canMove = false;
                }
                // turn goes back to initial player again
                this.isItHumanPlayersTurn = this.isItHumanPlayersTurn ? false : true;
                // check if initial player neither can make move
                if(this.canPlayerMakeMove()) {
                    // yes the player can make legal move
                    console.log('this.isItHumanPlayersTurn after flipping:', this.isItHumanPlayersTurn);
                    console.log('yes can move');
                    if (this.isItHumanPlayersTurn) {
                        this.playerHuman.canMove = true;
                    } else {
                        this.playerComputer.canMove = true;
                    }
                } else { 
                    // both player can no longer make move, game is over  
                    console.log('both players have no moves left, game is over');
                    this.isAllMovesExhaustedForBoth = true;

                    // Set the winner message
                    this.setWinnerMessage();
                }   
            }

            // if moves are not exhausted and it's computer's turn 
            if (!this.isAllMovesExhaustedForBoth && !this.isItHumanPlayersTurn) {
                console.log('not exhausted and computer\'s turn');
                this.randomMove();
            }
        }
    }

    setWinnerMessage() {
        let countComputer = this.playerComputer.piecesCount;
        let countHuman = this.playerHuman.piecesCount;

        if (countComputer === countHuman) {
            // It's a draw
            this.winnerMsg = 'It\'s a draw';
        } else if (countComputer > countHuman) {
            // Computer wins
            this.winnerMsg = 'Computer wins';
        } else if (countComputer < countHuman) {
            // Human wins
            this.winnerMsg = 'Congratulations, You Win';
        } else {
            // Something went wrong
            this.winnerMsg = 'Oops, something went wrong here';
        }
    }

    /**
     * Remove effects from all img pieces on the board
     */
    async removeEffects() {
        console.log('removeEffects');

        await this.sleep(this.removeEffectsDelay);
        console.log('below await removeEffects');
        
        // Remove effect from all pieces on board
        // find the img piece on this position
        let positionId;

        for (let outerCount = 0; outerCount < Y_AXIS_LENGTH; outerCount++) {
            for (let innerCount = 0; innerCount < X_AXIS_LENGTH; innerCount++) {
                // console.log('id:', (outerCount * SQUARE_LENGTH) + innerCount + 1);
                positionId = this.getPositionIdFromCoordinate(this.getCoordinateFromXY(innerCount, outerCount));
                console.log('positionId:', positionId);

                // get the div element with positionId
                let divElement = this.template.querySelector('div[data-id="' + positionId + '"');
                console.log('divElement:', divElement);

                // get the img element inside the div element
                let imgElement = divElement.querySelector('img');
                console.log('imgElement:', imgElement);

                // If there is an img on position, we want to remove effect
                if (imgElement) {
                    // do we need to remove class again?
                    imgElement.classList.remove("animate__shakeY", "animate__heartBeat");
                }         
            }
        }
    }

    sleep(milliseconds) {
        return new Promise(resolve => setTimeout(resolve, milliseconds));
    }

    async randomMove() {  
        await this.sleep(this.computerDelayMove);
        // Get 64 random numbers between 1 and 64, both included. Meaning 1-64 are ordered randomly.  
        let positionIdArray = this.getSetOfRandomNumbers(X_AXIS_LENGTH * Y_AXIS_LENGTH, X_AXIS_LENGTH * Y_AXIS_LENGTH);

        let positionIdByComputerRandom;

        for (let i of positionIdArray) {
            // console.log('random i', i);
            let coordinate = this.getCoordinateFromPositionId(i);
            if (this.isPositionLegal(coordinate, this.playerComputer)) {
                // if returning true then position was legal
                positionIdByComputerRandom = i;
                break;
            }
        }
        this.handlePositioning(positionIdByComputerRandom);
    }

    /**
     * Get a set of unique numbers where length of set will be sizeOfSet.
     * Numbers can range from 1 -> max, both included
     */
    getSetOfRandomNumbers(sizeOfSet, max) {
        // console.log('getSetOfRandomNumbers');
        // console.log('sizeOfSet', sizeOfSet);
        // console.log('max', max);
        const numberSet = new Set()
        while (numberSet.size < sizeOfSet) {
            numberSet.add(Math.floor(Math.random() * max) + 1);           
        }
        console.log('numberSet', numberSet);
        return numberSet;
    }

    flipPieces() {
        console.log('flipPieces');
        console.log('this.currentCoordinatesWherePiecesToFlip:', this.currentCoordinatesWherePiecesToFlip);

        // loop through entire list and flip
        this.currentCoordinatesWherePiecesToFlip.forEach(coordinate => {
            console.log('coordinate.x:', coordinate.x);
            console.log('coordinate.y:', coordinate.y);

            // Add effect to pieces that will be flipped
            // find the img piece on this position
            let positionId = this.getPositionIdFromCoordinate(coordinate);
            console.log('positionId:', positionId);

            // get the div element with positionId
            let divElement = this.template.querySelector('div[data-id="' + positionId + '"');
            console.log('divElement:', divElement);

            // get the img element inside the div element
            let imgElement = divElement.querySelector('img');
            console.log('imgElement:', imgElement);

            // First remove animate__heartBeat effect from img then Add effect to the img
            imgElement.classList.remove("animate__heartBeat");  // all img's are born with this effect (see html)
            imgElement.classList.add("animate__shakeY");

            if (this.isItHumanPlayersTurn) {
                this.board[coordinate.y][coordinate.x].imgUrl = this.playerHuman.pieceUrl;
                this.board[coordinate.y][coordinate.x].color = this.playerHuman.color;
            } else {
                this.board[coordinate.y][coordinate.x].imgUrl = this.playerComputer.pieceUrl;
                this.board[coordinate.y][coordinate.x].color = this.playerComputer.color;
            }   
            
        });
        // this.printBoard();
    }

    isPositionLegal(coordinate, player) {
        // this should hold array of potential coordinates to be flipped
        // but only hold values if the position is legal. 
        console.log('isPositionLegal: player color:', player.color);
        let isPositionLegal;
        if (this.isPositionOccupied(coordinate)) {
            console.log('occupied, meaning not legal');
            isPositionLegal = false;
        } else {
            console.log('free, now check if legal');

            let anyFlankingPossible = false;

            let anyFlankingPossibleUp = false;
            let anyFlankingPossibleDown = false;
            let anyFlankingPossibleRight = false;
            let anyFlankingPossibleLeft = false;
            
            let anyFlankingPossibleUpRight = false;
            let anyFlankingPossibleDownRight = false;
            let anyFlankingPossibleDownLeft = false;
            let anyFlankingPossibleUpLeft = false;

            // we need to check if we flank out opponent
            if (this.isCheckDirectionUpNeeded(coordinate)) {
                console.log('check up');

                let listUpCoordinates = [];
                for (let y = coordinate.y - 1; y >= 0; y--) {
                    listUpCoordinates.push(this.getCoordinateFromXY(coordinate.x, y));
                }
                console.log('listUpCoordinates:', listUpCoordinates);
                anyFlankingPossibleUp = this.isFlankingPossible(listUpCoordinates, null, player);
                console.log('anyFlankingPossibleUp:', anyFlankingPossibleUp);
            }
            
            if (this.isCheckDirectionDownNeeded(coordinate)) {
                console.log('check down');

                let listDownCoordinates = [];
                for (let y = coordinate.y + 1; y < SQUARE_LENGTH; y++) {
                    // listDownCoordinates.push(this.board[y][coordinate.x].color);
                    listDownCoordinates.push(this.getCoordinateFromXY(coordinate.x, y));
                }
                console.log('listDownCoordinates:', listDownCoordinates);
                
                anyFlankingPossibleDown = this.isFlankingPossible(listDownCoordinates, null, player);
                console.log('anyFlankingPossibleDown:', anyFlankingPossibleDown);
            }
            
            if (this.isCheckDirectionRightNeeded(coordinate)) {
                console.log('check right');

                let listRightCoordinates = [];
                for (let x = coordinate.x + 1; x < SQUARE_LENGTH; x++) {
                    // listRightCoordinates.push(this.board[coordinate.y][x].color);
                    listRightCoordinates.push(this.getCoordinateFromXY(x, coordinate.y));
                }
                console.log('listRightCoordinates:', listRightCoordinates);
                
                anyFlankingPossibleRight = this.isFlankingPossible(listRightCoordinates, null, player);
                console.log('anyFlankingPossibleRight:', anyFlankingPossibleRight);
            }
            
            if (this.isCheckDirectionLeftNeeded(coordinate)) {
                console.log('check left');

                let listLeftCoordinates = [];
                for (let x = coordinate.x - 1; x >= 0; x--) {
                    // listLeftCoordinates.push(this.board[coordinate.y][x].color);
                    listLeftCoordinates.push(this.getCoordinateFromXY(x, coordinate.y));
                }
                console.log('listLeftCoordinates:', listLeftCoordinates);
                
                anyFlankingPossibleLeft = this.isFlankingPossible(listLeftCoordinates, null, player);
                console.log('anyFlankingPossibleLeft:', anyFlankingPossibleLeft);
            }
            
            if (this.isCheckDirectionUpRightNeeded(coordinate)) {
                console.log('check up right');

                let listUpRightCoordinates = [];
                let x;
                let y;
                for (x = coordinate.x + 1, y = coordinate.y - 1; x < SQUARE_LENGTH && y >= 0; x++, y--) {
                    console.log('x:', x);
                    console.log('y:', y);
                    // listUpRightCoordinates.push(this.board[y][x].color);
                    listUpRightCoordinates.push(this.getCoordinateFromXY(x, y));
                }
                console.log('listUpRightCoordinates:', listUpRightCoordinates);
                
                anyFlankingPossibleUpRight = this.isFlankingPossible(listUpRightCoordinates, null, player);
                console.log('anyFlankingPossibleUpRight:', anyFlankingPossibleUpRight);
            }
            
            if (this.isCheckDirectionDownRightNeeded(coordinate)) {
                console.log('check down right');

                let listDownRightCoordinates = [];
                let x;
                let y;
                for (x = coordinate.x + 1, y = coordinate.y + 1; x < SQUARE_LENGTH && y < SQUARE_LENGTH; x++, y++) {
                    console.log('x:', x);
                    console.log('y:', y);
                    // listDownRightCoordinates.push(this.board[y][x].color);
                    listDownRightCoordinates.push(this.getCoordinateFromXY(x, y));
                }
                
                console.log('listDownRightCoordinates:', listDownRightCoordinates);
                
                anyFlankingPossibleDownRight = this.isFlankingPossible(listDownRightCoordinates, null, player);
                console.log('anyFlankingPossibleDownRight:', anyFlankingPossibleDownRight);
            }
            
            if (this.isCheckDirectionDownLeftNeeded(coordinate)) {
                console.log('check down left');

                let listDownLeftCoordinates = [];
                let x;
                let y;
                for (x = coordinate.x - 1, y = coordinate.y + 1; x >= 0 && y < SQUARE_LENGTH; x--, y++) {
                    console.log('x:', x);
                    console.log('y:', y);
                    // listDownLeftCoordinates.push(this.board[y][x].color);
                    listDownLeftCoordinates.push(this.getCoordinateFromXY(x, y));
                }
                console.log('listDownLeftCoordinates:', listDownLeftCoordinates);
                
                anyFlankingPossibleDownLeft = this.isFlankingPossible(listDownLeftCoordinates, null, player);
                console.log('anyFlankingPossibleDownLeft:', anyFlankingPossibleDownLeft);
            }
            
            if (this.isCheckDirectionUpLeftNeeded(coordinate)) {
                console.log('check Up left');

                let listUpLeftCoordinates = [];
                let x;
                let y;
                for (x = coordinate.x - 1, y = coordinate.y - 1; x >= 0 && y >= 0; x--, y--) {
                    console.log('x:', x);
                    console.log('y:', y);
                    // listUpLeftCoordinates.push(this.board[y][x].color);
                    listUpLeftCoordinates.push(this.getCoordinateFromXY(x, y));
                }
                console.log('listUpLeftCoordinates:', listUpLeftCoordinates);
                
                anyFlankingPossibleUpLeft = this.isFlankingPossible(listUpLeftCoordinates, null, player);
                console.log('anyFlankingPossibleUpLeft:', anyFlankingPossibleUpLeft);
            }

            if (anyFlankingPossibleUp || 
                anyFlankingPossibleDown || 
                anyFlankingPossibleRight || 
                anyFlankingPossibleLeft ||
                anyFlankingPossibleUpRight || 
                anyFlankingPossibleDownRight || 
                anyFlankingPossibleDownLeft || 
                anyFlankingPossibleUpLeft) {
                anyFlankingPossible = true;
            }
            isPositionLegal = anyFlankingPossible;
        }
        return isPositionLegal;
    }

    isFlankingPossible(coordinateList, possibleList, player) {
        console.log('isFlankingPossible');
        let opponentColor = player.color === WHITE ? BLACK : WHITE;
        console.log('player.color:', player.color);
        console.log('opponentColor:', opponentColor);
        console.log('coordinateList:', coordinateList);

        let listOfColors = [];
        coordinateList.forEach(coordinate => {
            listOfColors.push(this.board[coordinate.y][coordinate.x].color);
        });

        // forEach (let y = coordinate.y - 1; y >= 0; y--) {
        //     listUp.push(this.board[y][coordinate.x].color);
        // }
        console.log('listOfColors:', listOfColors);
        // anyFlankingPossibleUp = this.isFlankingPossible(listUp, player);
        // console.log('anyFlankingPossibleUp:', anyFlankingPossibleUp);

        // listDirection must be minimum 2 in length
        // there MUST be at least one of the player's color
        // player's color cannot be on index 0
        let allPiecesBetweenFlanksAreOpponentsColor = false;
        let coordinatesToFlip = [];
        if (listOfColors.length > 1) {
            let index = listOfColors.indexOf(player.color);
            console.log('index: ', index);
            if (index !== -1 && index > 0) {
                console.log('found flank possibility (players own color) on valid index');

                // let allPiecesBetweenFlanksAreOpponentsColor = false;
                // now check that all the items before the flank piece found on index are opponent's color
                for (let count = 0; count < index; count++) {
                    if (listOfColors[count] === opponentColor) {
                        allPiecesBetweenFlanksAreOpponentsColor = true;
                        coordinatesToFlip.push(coordinateList[count]);
                    } else {
                        allPiecesBetweenFlanksAreOpponentsColor = false;
                        break;
                    }
                }
            }
        }
        console.log('allPiecesBetweenFlanksAreOpponentsColor:', allPiecesBetweenFlanksAreOpponentsColor);

        // if allPiecesBetweenFlanksAreOpponentsColor then we want to add coordinatesToFlip to the overall list to be flipped
        if (allPiecesBetweenFlanksAreOpponentsColor) {
            // this.currentCoordinatesWherePiecesToFlip.push(coordinatesToFlip);
            this.currentCoordinatesWherePiecesToFlip = [...this.currentCoordinatesWherePiecesToFlip, ...coordinatesToFlip];
        }
        console.log('this.currentCoordinatesWherePiecesToFlip:', this.currentCoordinatesWherePiecesToFlip);

        return allPiecesBetweenFlanksAreOpponentsColor;
    }

    /**
     * Checks all positions on the board (all coordinates),
     * check if coordinate is legal to place the players piece on.
     * If at least one coordinate is valid, then move is possible.
     * @returns boolean true if player can make move, else false
     */
    canPlayerMakeMove() {
        let hasAtLeastOnePossibleMove = false;
        // check who's turn it is, isItHumanPlayersTurn
        let currentPlayer = this.isItHumanPlayersTurn ? this.playerHuman : this.playerComputer;
        // console.log('player to check:', currentPlayer.color);
        
        // loop through all coordinates on board
        // rows
        for (let outerCount = 0; outerCount < Y_AXIS_LENGTH; outerCount++) {
    
            // columns
            for (let innerCount = 0; innerCount < X_AXIS_LENGTH; innerCount++) {
                // console.log('id:', (outerCount * squareLength) + innerCount + 1);
                // let coordinate = {
                //     x: outerCount,
                //     y: innerCount
                // }
                let coordinate = this.getCoordinateFromXY(outerCount, innerCount);
                if (this.isPositionLegal(coordinate, currentPlayer)) {
                    hasAtLeastOnePossibleMove = true;
                    break;
                }
            }
            if (hasAtLeastOnePossibleMove) {
                break;
            }
        }
        // console.log('hasAtLeastOnePossibleMove:', hasAtLeastOnePossibleMove);
        return hasAtLeastOnePossibleMove;
    }

    /**
     * initialize board to array of array of null values
     */
     initializeEmptyBoard() {
        // console.log('initializeBoard');

        this.board = [];
        for (let outerCount = 0; outerCount < Y_AXIS_LENGTH; outerCount++) {

            let rowArray = [];

            for (let innerCount = 0; innerCount < X_AXIS_LENGTH; innerCount++) {
                // console.log('id:', (outerCount * SQUARE_LENGTH) + innerCount + 1);
                let pieceObj = {
                    id: (outerCount * Y_AXIS_LENGTH) + innerCount + 1,
                    x: innerCount, 
                    y: outerCount, 
                    color: null,
                    imgUrl: null
                };
                rowArray.push(pieceObj);
            }
            this.board.push(rowArray);
        }
    }

    /**
     * set the 4 starting pieces in center of board.
     */
    setInitialFourPiecesSetup() {
        // console.log('setInitialFourPiecesSetup');

        this.board[3][3].imgUrl = this.whitePieceUrl;
        this.board[3][3].color = WHITE;

        this.board[3][4].imgUrl = this.blackPieceUrl;
        this.board[3][4].color = BLACK;

        this.board[4][3].imgUrl = this.blackPieceUrl;
        this.board[4][3].color = BLACK;

        this.board[4][4].imgUrl = this.whitePieceUrl;
        this.board[4][4].color = WHITE;
    }

    isPositionOccupied(coordinate) {
        let color = this.board[coordinate.y][coordinate.x].color;
        if (color === null) {
            // console.log('color is null');
            return false;
        } else {
            // console.log('color not null');
            // console.log('color:', color);
            return true;
        }
    }

    /**
     * Direction: Up (|)
     */
    isCheckDirectionUpNeeded(coordinate) {
        console.log('----> (^) isCheckDirectionUpNeeded');
        if (coordinate.y < 2) {
            console.log('no');
            return false;
        } else {
            console.log('yes');
            return true;
        }
    }

    /**
     * Direction: Up-Right (/)
     */
    isCheckDirectionUpRightNeeded(coordinate) {
        console.log('----> (/^) isCheckDirectionUpRightNeeded');
        if (coordinate.y < 2 || coordinate.x > X_AXIS_LENGTH - 3) {
            console.log('no');
            return false;
        } else {
            console.log('yes');
            return true;
        }
    }

    /**
     * Direction: Right (->)
     */
    isCheckDirectionRightNeeded(coordinate) {
        console.log('----> (>) isCheckDirectionRightNeeded');
        if (coordinate.x > X_AXIS_LENGTH - 3) {
            console.log('no');
            return false;
        } else {
            console.log('yes');
            return true;
        }
    }

    /**
     * Direction: Down-Right (\)
     */
    isCheckDirectionDownRightNeeded(coordinate) {
        console.log('----> (\>) isCheckDirectionDownRightNeeded');
        if (coordinate.y > Y_AXIS_LENGTH - 3 || coordinate.x > X_AXIS_LENGTH - 3) {
            console.log('no');
            return false;
        } else {
            console.log('yes');
            return true;
        }
    }

    /**
     * Direction: Down (|)
     */
    isCheckDirectionDownNeeded(coordinate) {
        console.log('----> (v) isCheckDirectionDownNeeded');
        if (coordinate.y > Y_AXIS_LENGTH - 3) {
            console.log('no');
            return false;
        } else {
            console.log('yes');
            return true;
        }
    }

    /**
     * Direction: Down-Left (/)
     */
    isCheckDirectionDownLeftNeeded(coordinate) {
        console.log('----> (v/) isCheckDirectionDownLeftNeeded');
        if (coordinate.y > Y_AXIS_LENGTH - 3 || coordinate.x < 2) {
            console.log('no');
            return false;
        } else {
            console.log('yes');
            return true;
        }
    }

    /**
     * Direction: Left (<-)
     */
    isCheckDirectionLeftNeeded(coordinate) {
        console.log('----> (<) isCheckDirectionLeftNeeded');
        if (coordinate.x < 2) {
            console.log('no');
            return false;
        } else {
            console.log('yes');
            return true;
        }
    }

    /**
     * Direction: Up-Left (\)
     */
    isCheckDirectionUpLeftNeeded(coordinate) {
        console.log('----> (^\) isCheckDirectionUpLeftNeeded');
        if (coordinate.y < 2 || coordinate.x < 2) {
            console.log('no');
            return false;
        } else {
            console.log('yes');
            return true;
        }
    }

    getCoordinateFromPositionId(positionId) {
        let xCoordinate = (positionId - 1) % X_AXIS_LENGTH;
        let yCoordinate = Math.ceil(positionId / Y_AXIS_LENGTH) - 1;
        console.log('xCoordinate:', xCoordinate);
        console.log('yCoordinate:', yCoordinate);
        let coordinate = {
            x: xCoordinate,
            y: yCoordinate
        }
        return coordinate;
    }

    getCoordinateFromXY(xCoordinate, yCoordinate) {
        let coordinate = {
            x: xCoordinate,
            y: yCoordinate
        }
        return coordinate;
    }

    /**
     * return the positionId on the board derived from the
     * coordinate passed to method. 
     * positionId counts from 1 on coordinate 0, 0
     * Formula is: (Y * SQUARE_LENGTH) + X + 1
     * @param coordinate with x and y
     */
    getPositionIdFromCoordinate(coordinate) {
        let positionId = (coordinate.y * SQUARE_LENGTH) + coordinate.x + 1;
        return positionId;
    }

    printBoard() {
        // cols
        for (let outerCount = 0; outerCount < Y_AXIS_LENGTH; outerCount++) {
            // rows
            for (let innerCount = 0; innerCount < X_AXIS_LENGTH; innerCount++) {
                console.log('x:', innerCount);
                console.log('y:', outerCount);
                console.log('id:', (outerCount * Y_AXIS_LENGTH) + innerCount + 1);
                console.log('color:', this.board[outerCount][innerCount].color);
                // this.board[outerCount][innerCount].color
            }
        }
    }

    setCountForPlayers() {
        // console.log('getCountForPlayer');

        let counterHuman = 0;
        let counterComputer = 0;        
        // cols
        for (let outerCount = 0; outerCount < Y_AXIS_LENGTH; outerCount++) {
            // rows
            for (let innerCount = 0; innerCount < X_AXIS_LENGTH; innerCount++) {
                // console.log('color:', this.board[outerCount][innerCount].color);
                if (this.board[outerCount][innerCount].color === this.playerHuman.color) {
                    counterHuman++;
                } else if (this.board[outerCount][innerCount].color === this.playerComputer.color) {
                    counterComputer++;
                } 
            }
        }

        this.playerHuman.piecesCount = counterHuman;
        this.playerComputer.piecesCount = counterComputer;
    }

    // get the form factor
    get isDesktop() {
        switch(FORM_FACTOR) {
            case 'Large':
                return true;
            case 'Medium':
                return false;
            case 'Small':
                return false;
            default:
        }
    }

}