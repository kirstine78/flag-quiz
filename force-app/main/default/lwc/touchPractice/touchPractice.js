import { LightningElement } from 'lwc';
// import your images from static resources
import HORSE_IMAGES from '@salesforce/resourceUrl/touchPracticeHorses';

export default class TouchPractice extends LightningElement {
    
    horseImage1 = HORSE_IMAGES + '/touchPracticeHorses/horse1.jpg';
    horseImage2 = HORSE_IMAGES + '/touchPracticeHorses/arabian.jpeg';
    horseImage3 = HORSE_IMAGES + '/touchPracticeHorses/kwpn.jpg';

}