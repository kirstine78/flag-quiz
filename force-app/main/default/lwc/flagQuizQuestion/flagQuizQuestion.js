/**
 * Author: Kirstine Broerup Nielsen
 */

import { LightningElement, api } from 'lwc';

export default class FlagQuizQuestion extends LightningElement {
    @api questionObject;
    userAnswer;
    score = 0;
    numberCounter = 1;

    /**
     * Called when user click an answer.
     * Dispatches a custom event to indicate that an answer was clicked.
     */
    handleAnswerClick(event) {
        this.toggleDivAnswerBackgroundColor(event);
        
        this.userAnswer = event.target.dataset.id;
        // console.log('this.userAnswer', this.userAnswer);

        // dispatch event to indicate that Next Question is ok to show
        this.dispatchEvent(new CustomEvent('answerisclicked', {
            detail: true
        }));
    }

    /**
     * Toggle the divs that represents the answers. 
     * The selected div answer should get selectedanswer class added to show colored background.
     */
    toggleDivAnswerBackgroundColor(event) {
        const answerElements = this.template.querySelectorAll('.questionanswer');

        Array.from(answerElements).forEach(item => {
            // an item is a div in our example
            if (item.classList.contains('selectedanswer')) {
                if (item.innerText !== event.target.dataset.id) {
                    item.classList.remove('selectedanswer');
                }
            } else {
                if (item.innerText === event.target.dataset.id) {
                    item.classList.add('selectedanswer');
                }
            }
        });
    }

    /**
     * Removes selectedanswer class from any div answer having this applied currently.
     */
    @api
    removeBackgroundColor() {
        const answerElements = this.template.querySelectorAll('.questionanswer');

        Array.from(answerElements).forEach(item => {
            // an item is a div in our example
            if (item.classList.contains('selectedanswer')) {
                item.classList.remove('selectedanswer');
            }
        });
    }

    /**
     * Checks if users chosen answer matches the correct answer of the question object currently
     * in question. 
     * Return true if users answer is correct, else false.
     */
    isUserAnswerCorrect() {        
        let correctAnswer = this.questionObject.correctAnswer;
        // console.log('correct:', correctAnswer === this.userAnswer);
        return correctAnswer === this.userAnswer;
    }

    /**
     * Updates the game score by incrementing by one if users answer is correct.
     * Invokes method that will send the score to parent component.
     */
    @api
    updateScore() {
        if (this.isUserAnswerCorrect()) {
            this.score++;
        }

        // send event to parent
        this.sendScore();
    }

    /**
     * Dispatch event that contains details about the current score.
     */
    sendScore() {
        this.dispatchEvent(new CustomEvent('scoreupdate', {
            detail: this.score
        }));
    }

    /**
     * Dispatches custom event with details about the answer to a question that user
     * just has committed; by committed is meant use has clicked an answer and clicked the Next/Finish button.
     */
    @api
    sendResultRow() {
        this.dispatchEvent(new CustomEvent('resultrowaddition', {
            detail: {
                number: this.numberCounter,
                country: this.questionObject.country,
                imageUrl: this.questionObject.imageUrl,
                correctAnswer: this.questionObject.correctAnswer,
                userAnswer: this.userAnswer,
                isCorrect: this.questionObject.correctAnswer === this.userAnswer
            }
        }));
        this.numberCounter++;
    }
}
