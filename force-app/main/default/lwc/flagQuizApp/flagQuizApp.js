/**
 * Author: Kirstine Broerup Nielsen
 */

import { LightningElement, track } from 'lwc';
// import { answersPool } from './answersPool';
import { questionsPool } from './questionsPool';

const MAX_INDEX = 3;
const SIZE_OF_ANSWERS_ARRAY_FOR_GUI = 4;
const NUMBER_OF_QUESTIONS_PER_GAME = 8;

export default class FlagQuizApp extends LightningElement {
    numberOfQuestionsPerGame = NUMBER_OF_QUESTIONS_PER_GAME;
    isPlayModeByCountry = false; // use
    isQuizStarted = false;  // use
    shouldResultShow = false;  // use
    isAnswerSelected = false;  // use
    correctAnswersCount = 0;  // use
    labelNextButton = '';  // use
    resultMatrix = [];
    gameQuestionsList = [];       // only the selected questions from questionsPool to be used in game

    currentQuestionObject;
    currentQuestionNumber = null;
    questionCountOnGUI = 0;

    questionsPool = questionsPool;

    /**
     * Starts the quiz by country when button in pressed
     */
    startQuizByCountryHandler() {
        this.isPlayModeByCountry = true;
        this.startQuizHandler();
    }

    /**
     * Starts the quiz by city when button in pressed
     */
    startQuizByCityHandler() {
        this.isPlayModeByCountry = false;
        this.startQuizHandler();
    }

    /**
     * Start handler
     */
    startQuizHandler() {
        this.buildGameQuestionsList();
        this.isQuizStarted = true;
        this.currentQuestionNumber = 0;
        this.questionCountOnGUI = this.currentQuestionNumber + 1;
        this.currentQuestionObject = this.gameQuestionsList[this.currentQuestionNumber];
    }

    /**
     * Build the gameQuestionsList which will contain only the question objects we need 
     * for this game.
     */
    buildGameQuestionsList() {
        // gameQuestionsList has to consist of NUMBER_OF_QUESTIONS_PER_GAME question objects
        // We have to choose randomly from the questionsPool
        let randomUniqueNumbersSet = this.getSetOfRandomNumbers(NUMBER_OF_QUESTIONS_PER_GAME, this.questionsPool.length - 1);
        for (let i of randomUniqueNumbersSet) {
            // console.log(i);
            
            // hack, first convert to JSON string, then convert back to JavaScript object
            this.gameQuestionsList.push(JSON.parse(JSON.stringify(this.questionsPool[i])));
        }

        // check the Mode of which the user chose to play
        if (this.isPlayModeByCountry) {
            // mode is by Country
            // populate correctAnswer and wrongAnswers for each question object in gameQuestionsList        
            this.gameQuestionsList.forEach(questionObject => {
                let singleCorrectAnswerForQuestion = questionObject.country;
                let wrongAnswersPoolForQuestionObject = this.getWrongAnswersPoolForQuestion(questionObject.country);
                let threeWrongAnswersForQuestion = this.getThreeWrongAnswersForQuestion(wrongAnswersPoolForQuestionObject);

                // assign to this current questionObject
                questionObject.correctAnswer = singleCorrectAnswerForQuestion;
                questionObject.wrongAnswers = threeWrongAnswersForQuestion;
                questionObject.shuffledAnswers = this.shuffleAnswersArray(singleCorrectAnswerForQuestion, threeWrongAnswersForQuestion);

            });
        } else {
            // mode is by City
            // populate correctAnswer and wrongAnswers for each question object in gameQuestionsList    
            // console.log('else');    
            this.gameQuestionsList.forEach(questionObject => {
                let singleCorrectAnswerForQuestion = this.getSingleCorrectAnswerForQuestion(questionObject.country);
                let wrongAnswersPoolForQuestionObject = this.getWrongAnswersPoolForQuestion(questionObject.country);
                let threeWrongAnswersForQuestion = this.getThreeWrongAnswersForQuestion(wrongAnswersPoolForQuestionObject);

                // assign to this current questionObject
                questionObject.correctAnswer = singleCorrectAnswerForQuestion;
                questionObject.wrongAnswers = threeWrongAnswersForQuestion;
                questionObject.shuffledAnswers = this.shuffleAnswersArray(singleCorrectAnswerForQuestion, threeWrongAnswersForQuestion);

                // console.log('after shuffle');    
            });
        }

        // console.log('After loop this.gameQuestionsList', this.gameQuestionsList);
    }

    /**
     * Get one single correct answer for a question. 
     * Answer is a city that belongs to the countryToFind.
     */
    getSingleCorrectAnswerForQuestion(countryToFind) {
        // console.log('countryToFind', countryToFind);

        let singleCorrectAnswerForQuestion = '';
        // search in the questionsPool that has been imported
        questionsPool.forEach(questionObject => {
            // console.log('questionObject', questionObject);
            // check if the country in the questionObject matches the country passed in
            if (countryToFind === questionObject.country) {
                // an questionObject has a list of cityAnswerOptions, we must randomly pick only one
                // between 0 [included] and questionObject.cityAnswerOptions.length [excluded]
                let index = this.getRandomInt(questionObject.cityAnswerOptions.length);
                singleCorrectAnswerForQuestion = questionObject.cityAnswerOptions[index];
            }
        });
        return singleCorrectAnswerForQuestion;

        // let singleCorrectAnswerForQuestion = '';
        // // search in the answersPool that has been imported
        // answersPool.forEach(answerObject => {
        //     // console.log('answerObject', answerObject);
        //     // check if the country in the answer matches the country passed in
        //     if (countryToFind === answerObject.country) {
        //         // an answerObject has a list of answers, we must randomly pick only one
        //         // between 0 [included] and answerObject.answers.length [excluded]
        //         let index = this.getRandomInt(answerObject.answers.length);
        //         singleCorrectAnswerForQuestion = answerObject.answers[index];
        //     }
        // });
        // return singleCorrectAnswerForQuestion;
    }

    /**
     * Get a pool of wrong answers. 
     * Pool will contain answers that are cities that doesn't belong to the countryToAvoid. 
     * There will be maximum one city per country.
     */
    getWrongAnswersPoolForQuestion(countryToAvoid) {
        // console.log('countryToAvoid', countryToAvoid);

        let wrongAnswersForQuestion = [];
        questionsPool.forEach(questionObject => {
            // check that the country in the questionObject doesn't match the countryToAvoid
            if (countryToAvoid !== questionObject.country) {
                if (this.isPlayModeByCountry) {
                    wrongAnswersForQuestion.push(questionObject.country);
                } else {
                    // play is City mode
                    // an questionObject has a list of cityAnswerOptions, we must randomly pick only one
                    // between 0 [included] and questionObject.cityAnswerOptions.length [excluded]
                    let index = this.getRandomInt(questionObject.cityAnswerOptions.length);
                    wrongAnswersForQuestion.push(questionObject.cityAnswerOptions[index]);
                }
            }
        });
        return wrongAnswersForQuestion;

        // let wrongAnswersForQuestion = [];
        // answersPool.forEach(answerObject => {
        //     // check that the country in the answer doesn't match the countryToAvoid
        //     if (countryToAvoid !== answerObject.country) {
        //         if (this.isPlayModeByCountry) {
        //             wrongAnswersForQuestion.push(answerObject.country);
        //         } else {
        //             // an answerObject has a list of answers, we must randomly pick only one
        //             // between 0 [included] and answerObject.answers.length [excluded]
        //             let index = this.getRandomInt(answerObject.answers.length);
        //             wrongAnswersForQuestion.push(answerObject.answers[index]);
        //         }
        //     }
        // });
        // return wrongAnswersForQuestion;
    }

    /**
     * We pass in a poolOfWrongAnswers and we return a list of only 3 wrong answers,
     * randomly selected from poolOfWrongAnswers
     */
    getThreeWrongAnswersForQuestion(poolOfWrongAnswers) {
        // console.log('getThreeWrongAnswersForQuestion'); 
        // we only want to select three answers from the poolOfWrongAnswers
        let indexesToUse = this.getSetOfRandomNumbers(3, poolOfWrongAnswers.length - 1) ;
        let threeWrongAnswersForQuestion = [];
        for (let i of indexesToUse) {
            // console.log(i);
            threeWrongAnswersForQuestion.push(poolOfWrongAnswers[i]);
        }
        // console.log('threeWrongAnswersForQuestion', threeWrongAnswersForQuestion);
        return threeWrongAnswersForQuestion;
    }    

    /**
     * We combine a string with array into another array. 
     * We ensure that the order is random.
     * Return the shuffled array.
     */
    shuffleAnswersArray(singleCorrectAnswer, listOfThreeWrongAnswers) {
        let comboAnswersArray = [...listOfThreeWrongAnswers, singleCorrectAnswer];
        let comboAnswersArrayShuffled = [];

        let indexArray = this.getSetOfRandomNumbers(SIZE_OF_ANSWERS_ARRAY_FOR_GUI, MAX_INDEX);
        // console.log('indexArray', indexArray);
        
        for (let i of indexArray) {
            // console.log('i', i);
            comboAnswersArrayShuffled.push(comboAnswersArray[i]);
        }
        // console.log('comboAnswersArrayShuffled', comboAnswersArrayShuffled);
        return comboAnswersArrayShuffled;
    }

    /**
     * Next/Finish button handler.
     * Calls methods in child component that handles score count, 
     * and keep track of the result of one question.
     */
    nextQuestionHandler() {
        this.isAnswerSelected = false;

        this.template.querySelector('c-flag-quiz-question').updateScore();
        this.template.querySelector('c-flag-quiz-question').sendResultRow();        

        this.updateCurrentQuestionNumber();
        if (this.currentQuestionNumber !== null) {
            this.currentQuestionObject = this.gameQuestionsList[this.currentQuestionNumber];
        } else {
            this.currentQuestionObject = null;
        }
        this.template.querySelector('c-flag-quiz-question').removeBackgroundColor();
    }

    /**
     * Updates the number that keep track of the current question. 
     */
    updateCurrentQuestionNumber() {
        // console.log('this.gameQuestionsList.length', this.gameQuestionsList.length);
        // console.log('BEFORE this.currentQuestionNumber', this.currentQuestionNumber);

        if (this.gameQuestionsList.length - 1 === this.currentQuestionNumber) {
            this.currentQuestionNumber = null;
            this.questionCountOnGUI = 0;
            // we need to display Result since end of questions have been reached
            this.shouldResultShow = true;
        } else {
            this.currentQuestionNumber++;
            this.questionCountOnGUI = this.currentQuestionNumber + 1;
        }
        // console.log('AFTER this.currentQuestionNumber', this.currentQuestionNumber);
    } 

    /**
     * Get a set of unique numbers where length of set will be sizeOfSet.
     * Numbers can range from 0 -> max, both included
     */
    getSetOfRandomNumbers(sizeOfSet, max) {
        // console.log('getSetOfRandomNumbers');
        // console.log('sizeOfSet', sizeOfSet);
        // console.log('max', max);
        const numberSet = new Set()
        while (numberSet.size < sizeOfSet) {
            numberSet.add(Math.floor(Math.random() * (max + 1)));            
        }
        // console.log('numberSet', numberSet);
        return numberSet;
    }

    getRandomInt(maxWhichIsExcluded) {
        return Math.floor(Math.random() * maxWhichIsExcluded);
    }

    /**
     * eventhandler that sets the correct answers count
     */
    handleScoreUpdate(event) {
        this.correctAnswersCount = event.detail;
    }

    /**
     * Update the button label. Logic to decide if 
     * Next Question or Finish should be displayed.
     */
    handleAnswerWasClicked(event) {
        this.isAnswerSelected = event.detail;

        if (this.currentQuestionNumber < this.gameQuestionsList.length - 1) {
            this.labelNextButton = 'Next Question';
        } else {
            this.labelNextButton = 'Finish';
        }
    }

    /**
     * Add the latest result for one question to the resultMatrix
     */
    handleResultRowAddition(event) {        
        this.resultMatrix.push(event.detail);
        // console.log('this.resultMatrix', this.resultMatrix);
    }

    doneHandler() {
        this.reset();
        this.isQuizStarted = false;
    }

    playAgainHandler() {
        this.reset();
        this.isQuizStarted = true;
        this.startQuizHandler();
    }

    reset() {
        this.correctAnswersCount = 0;
        this.shouldResultShow = false;
        this.isAnswerSelected = false;
        this.gameQuestionsList = [];
        this.currentQuestionObject = null;
        this.resultMatrix = [];
    }

    /**
     * Return boolean, true if question should be shown, false if 
     * question shouln't be shown
     */
    get shouldQuestionBeShown() {
        return this.currentQuestionNumber === null ? false : true;
    }
}