/**
 * Pool of multiple answer objects in it.
 * For each game we will select an object where the country matches, and from this
 * object we will select exactly one of its answers. This answer will be the correct answer. 
 * For each game we will select all objects where the country doesn't match, and from each
 * object we will select exactly one of its answers. This will create a pool of wrong answers 
 * from which we later will randomly select only 3 answers from per question.
 */
const answersPool = [
    
    {   
        country: "Andorra",
        answers: ["Andorra la Vella"]
    },
    {   
        country: "United Arab Emirates",
        answers: ["Abu Dhabi"]
    },
    {   
        country: "Afghanistan",
        answers: ["Kabul"]
    },
    {   
        country: "Antigua and Barbuda",
        answers: ["St. John's"]
    },
    {   
        country: "Anguilla",
        answers: ["The Valley"]
    },
    {   
        country: "Albania",
        answers: ["Tirana"]
    },
    {   
        country: "Armenia",
        answers: ["Yerevan"]
    },
    {   
        country: "Angola",
        answers: ["Luanda"]
    },
    {   
        country: "Argentina",
        answers: ["Buenos Aires"]
    },
    {   
        country: "American Samoa",
        answers: ["Pago Pago"]
    },
    {   
        country: "Austria",
        answers: ["Vienna"]
    },
    {   
        country: "Australia",
        answers: ["Canberra", "Melbourne", "Sydney", "Brisbane"]
    },
    {   
        country: "Aruba",
        answers: ["Oranjestad"]
    },
    {   
        country: "Azerbaijan",
        answers: ["Baku"]
    },
    {   
        country: "Bosnia and Herzegovina",
        answers: ["Sarajevo"]
    },
    {   
        country: "Barbados",
        answers: ["Bridgetown"]
    },
    {   
        country: "Bangladesh",
        answers: ["Dhaka"]
    },
    {   
        country: "Belgium",
        answers: ["Brussels"]
    },
    {   
        country: "Burkina Faso",
        answers: ["Ouagadougou"]
    },
    {   
        country: "Bulgaria",
        answers: ["Sofia"]
    },
    {   
        country: "Bahrain",
        answers: ["Manama"]
    },
    {   
        country: "Burundi",
        answers: ["Bujumbura"]
    },
    {   
        country: "Benin",
        answers: ["Porto-Novo"]
    },
    {   
        country: "Bermuda",
        answers: ["Hamilton"]
    },
    {   
        country: "Brunei Darussalam",
        answers: ["Bandar Seri Begawan"]
    },
    {   
        country: "Bolivia",
        answers: ["Sucre"]
    },
    {   
        country: "Brazil",
        answers: ["Rio de Janeiro"]
    },
    {   
        country: "Bahamas",
        answers: ["Nassau"]
    },
    {   
        country: "Bhutan",
        answers: ["Thimphu"]
    },
    {   
        country: "Botswana",
        answers: ["Gaborone"]
    },
    {   
        country: "Belarus",
        answers: ["Minsk"]
    },
    {   
        country: "Belize",
        answers: ["Belmopan"]
    },
    {   
        country: "Canada",
        answers: ["Ottawa"]
    },
    {   
        country: "The Democratic Republic of the Congo",
        answers: ["Kinshasa"]
    },
    {   
        country: "Central African Republic",
        answers: ["Bangui"]
    },
    {   
        country: "Congo",
        answers: ["Brazzaville"]
    },
    {   
        country: "Switzerland",
        answers: ["Bern"]
    },
    {   
        country: "Ivory Coast",
        answers: ["Yamoussoukro"]
    },
    {   
        country: "Cook Islands",
        answers: ["Avarua"]
    },
    {   
        country: "Chile",
        answers: ["Santiago"]
    },
    {   
        country: "Cameroon",
        answers: ["Douala"]
    },
    {   
        country: "China",
        answers: ["Beijing"]
    },
    {   
        country: "Colombia",
        answers: ["Cartagena"]
    },
    {   
        country: "Costa Rica",
        answers: ["San Isidro de El General"]
    },
    {   
        country: "Cuba",
        answers: ["Havana"]
    },
    {   
        country: "Cape Verde",
        answers: ["Praia"]
    },
    {   
        country: "Christmas Island",
        answers: ["Flying Fish Cove"]
    },
    {   
        country: "Cyprus",
        answers: ["Nicosia"]
    },
    {   
        country: "Czech Republic",
        answers: ["Prague"]
    },
    {   
        country: "Germany",
        answers: ["Berlin"]
    },
    {   
        country: "Djibouti",
        answers: ["Djibouti"]
    },
    {   
        country: "Denmark",
        answers: ["Copenhagen", "Esbjerg", "Aarhus", "Odense"]
    },
    {   
        country: "Dominica",
        answers: ["Roseau"]
    },
    {   
        country: "Dominican Republic",
        answers: ["Santo Domingo"]
    },
    {   
        country: "Algeria",
        answers: ["Algiers"]
    },
    {   
        country: "Ecuador",
        answers: ["Quito"]
    },
    {   
        country: "Estonia",
        answers: ["Tallinn"]
    },
    {   
        country: "Egypt",
        answers: ["Cairo"]
    },
    {   
        country: "Western Sahara",
        answers: ["Laayoune"]
    },
    {   
        country: "Eritrea",
        answers: ["Asmara"]
    },
    {   
        country: "Spain",
        answers: ["Madrid"]
    },
    {   
        country: "Ethiopia",
        answers: ["Addis Ababa"]
    },
    {   
        country: "Finland",
        answers: ["Helsinki"]
    },
    {   
        country: "Fiji",
        answers: ["Suva"]
    },
    {   
        country: "Falkland Islands (Malvinas)",
        answers: ["Stanley"]
    },
    {   
        country: "Faroe Islands",
        answers: ["Thorshavn"]
    },
    {   
        country: "France",
        answers: ["Paris"]
    },
    {   
        country: "Gabon",
        answers: ["Libreville"]
    },
    {   
        country: "United Kingdom",
        answers: ["London"]
    },
    {   
        country: "Grenada",
        answers: ["St. George's"]
    },
    {   
        country: "Georgia",
        answers: ["Tbilisi"]
    },
    {   
        country: "French Guiana",
        answers: ["Cayenne"]
    },
    {   
        country: "Guernsey",
        answers: ["St. Peter Port"]
    },
    {   
        country: "Ghana",
        answers: ["Accra"]
    },
    {   
        country: "Gibraltar",
        answers: ["Gibraltar"]
    },
    {   
        country: "Greenland",
        answers: ["Nuuk"]
    },
    {   
        country: "Gambia",
        answers: ["Banjul"]
    },
    {   
        country: "Guinea",
        answers: ["Conakry"]
    },
    {   
        country: "Equatorial Guinea",
        answers: ["Malabo"]
    },
    {   
        country: "Greece",
        answers: ["Athens"]
    },
    {   
        country: "South Georgia and the South Sandwich Islands",
        answers: ["Grytviken"]
    },
    {   
        country: "Guatemala",
        answers: ["Guatemala City"]
    },
    {   
        country: "Guam",
        answers: ["Dededo"]
    },
    {   
        country: "Guinea-Bissau",
        answers: ["Bissau"]
    },
    {   
        country: "Guyana",
        answers: ["Georgetown"]
    },
    {   
        country: "Honduras",
        answers: ["Tegucigalpa"]
    },
    {   
        country: "Croatia",
        answers: ["Zagreb"]
    },
    {   
        country: "Haiti",
        answers: ["Port-au-Prince"]
    },
    {   
        country: "Hungary",
        answers: ["Budapest"]
    },
    {   
        country: "Indonesia",
        answers: ["Jakarta"]
    },
    {   
        country: "Ireland",
        answers: ["Dublin"]
    },
    {   
        country: "Israel",
        answers: ["Jerusalem"]
    },
    {   
        country: "Isle of Man",
        answers: ["Douglas"]
    },
    {   
        country: "India",
        answers: ["New Delhi", "Mumbai", "Bangalore", "Kolkata", "Jaipur"]
    },
    {   
        country: "Iraq",
        answers: ["Baghdad"]
    },
    {   
        country: "Iran",
        answers: ["Tehran"]
    },
    {   
        country: "Iceland",
        answers: ["Reykjavik"]
    },
    {   
        country: "Italy",
        answers: ["Rome"]
    },
    {   
        country: "Jersey",
        answers: ["St. Helier"]
    },
    {   
        country: "Jamaica",
        answers: ["Kingston"]
    },
    {   
        country: "Jordan",
        answers: ["Amman"]
    },
    {   
        country: "Japan",
        answers: ["Tokyo"]
    },
    {   
        country: "Kenya",
        answers: ["Nairobi"]
    },
    {   
        country: "Kyrgyzstan",
        answers: ["Bishkek"]
    },
    {   
        country: "Cambodia",
        answers: ["Phnom Penh"]
    },
    {   
        country: "Kiribati",
        answers: ["Tarawa"]
    },
    {   
        country: "Comoros",
        answers: ["Moroni"]
    },
    {   
        country: "Saint Kitts and Nevis",
        answers: ["Basseterre"]
    },
    {   
        country: "Democratic People's Republic of Korea",
        answers: ["Pyongyang"]
    },
    {   
        country: "Republic of Korea",
        answers: ["Seoul"]
    },
    {   
        country: "Kuwait",
        answers: ["Kuwait City"]
    },
    {   
        country: "Cayman Islands",
        answers: ["George Town"]
    },
    {   
        country: "Kazakhstan",
        answers: ["Astana"]
    },
    {   
        country: "Lao People's Democratic Republic",
        answers: ["Vientiane"]
    },
    {   
        country: "Lebanon",
        answers: ["Beirut"]
    },
    {   
        country: "Saint Lucia",
        answers: ["Castries"]
    },
    {   
        country: "Liechtenstein",
        answers: ["Vaduz"]
    },
    {   
        country: "Sri Lanka",
        answers: ["Sri Jayawardenapura Kotte", "Moratuwa", "Negombo", "Trincomalee", "Dambulla"]
    },
    {   
        country: "Liberia",
        answers: ["Monrovia"]
    },
    {   
        country: "Lesotho",
        answers: ["Maseru"]
    },
    {   
        country: "Lithuania",
        answers: ["Vilnius"]
    },
    {   
        country: "Luxembourg",
        answers: ["Luxembourg"]
    },
    {   
        country: "Latvia",
        answers: ["Riga"]
    },
    {   
        country: "Libya",
        answers: ["Tripoli"]
    },
    {   
        country: "Morocco",
        answers: ["Rabat"]
    },
    {   
        country: "Monaco",
        answers: ["Monaco"]
    },
    {   
        country: "Moldova",
        answers: ["Chisinau"]
    },
    {   
        country: "Montenegro",
        answers: ["Podgorica"]
    },
    {   
        country: "Madagascar",
        answers: ["Antananarivo"]
    },
    {   
        country: "Marshall Islands",
        answers: ["Majuro"]
    },
    {   
        country: "Macedonia",
        answers: ["Skopje"]
    },
    {   
        country: "Mali",
        answers: ["Bamako"]
    },
    {   
        country: "Myanmar",
        answers: ["Naypyidaw"]
    },
    {   
        country: "Mongolia",
        answers: ["Ulaanbaatar"]
    },
    {   
        country: "Northern Mariana Islands",
        answers: ["Saipan"]
    },
    {   
        country: "Mauritania",
        answers: ["Nouakchott"]
    },
    {   
        country: "Montserrat",
        answers: ["Plymouth"]
    },
    {   
        country: "Malta",
        answers: ["Valletta"]
    },
    {   
        country: "Mauritius",
        answers: ["Port Louis"]
    },
    {   
        country: "Maldives",
        answers: ["Hinnavaru"]
    },
    {   
        country: "Malawi",
        answers: ["Lilongwe"]
    },
    {   
        country: "Mexico",
        answers: ["Mexico City"]
    },
    {   
        country: "Malaysia",
        answers: ["Kuala Lumpur"]
    },
    {   
        country: "Mozambique",
        answers: ["Maputo"]
    },
    {   
        country: "Namibia",
        answers: ["Windhoek"]
    },
    {   
        country: "New Caledonia",
        answers: ["Le Mont-Dore"]
    },
    {   
        country: "Niger",
        answers: ["Niamey"]
    },
    {   
        country: "Norfolk Island",
        answers: ["Kingston"]
    },
    {   
        country: "Nigeria",
        answers: ["Abuja"]
    },
    {   
        country: "Nicaragua",
        answers: ["Managua"]
    },
    {   
        country: "Netherlands",
        answers: ["Amsterdam"]
    },
    {   
        country: "Norway",
        answers: ["Oslo"]
    },
    {   
        country: "Nepal",
        answers: ["Kathmandu"]
    },
    {   
        country: "Nauru",
        answers: ["Yaren"]
    },
    {   
        country: "Niue",
        answers: ["Alofi"]
    },
    {   
        country: "New Zealand",
        answers: ["Wellington"]
    },
    {   
        country: "Oman",
        answers: ["Muscat"]
    },
    {   
        country: "Panama",
        answers: ["Panama City"]
    },
    {   
        country: "Peru",
        answers: ["Lima"]
    },
    {   
        country: "French Polynesia",
        answers: ["Papeete"]
    },
    {   
        country: "Papua New Guinea",
        answers: ["Port Moresby"]
    },
    {   
        country: "Philippines",
        answers: ["Manila"]
    },
    {   
        country: "Pakistan",
        answers: ["Islamabad"]
    },
    {   
        country: "Poland",
        answers: ["Warsaw"]
    },
    {   
        country: "Saint Pierre and Miquelon",
        answers: ["St. Pierre"]
    },
    {   
        country: "Puerto Rico",
        answers: ["San Juan"]
    },
    {   
        country: "State of Palestine",
        answers: ["Jerusalem"]
    },
    {   
        country: "Portugal",
        answers: ["Lisbon"]
    },
    {   
        country: "Palau",
        answers: ["Ngerulmud"]
    },
    {   
        country: "Paraguay",
        answers: ["Ciudad del Este"]
    },
    {   
        country: "Qatar",
        answers: ["Doha"]
    },
    {   
        country: "Romania",
        answers: ["Bucharest"]
    },
    {   
        country: "Serbia",
        answers: ["Belgrade"]
    },
    {   
        country: "Russian Federation",
        answers: ["Moscow"]
    },
    {   
        country: "Rwanda",
        answers: ["Kigali"]
    },
    {   
        country: "Saudi Arabia",
        answers: ["Riyadh"]
    },
    {   
        country: "Solomon Islands",
        answers: ["Honiara"]
    },
    {   
        country: "Seychelles",
        answers: ["Victoria"]
    },
    {   
        country: "Sudan",
        answers: ["Khartoum"]
    },
    {   
        country: "Sweden",
        answers: ["Stockholm"]
    },
    {   
        country: "Singapore",
        answers: ["Singapore"]
    },
    {   
        country: "Slovenia",
        answers: ["Ljubljana"]
    },
    {   
        country: "Slovakia",
        answers: ["Bratislava"]
    },
    {   
        country: "Sierra Leone",
        answers: ["Freetown"]
    },
    {   
        country: "San Marino",
        answers: ["San Marino"]
    },
    {   
        country: "Senegal",
        answers: ["Dakar"]
    },
    {   
        country: "Somalia",
        answers: ["Mogadishu"]
    },
    {   
        country: "Suriname",
        answers: ["Paramaribo"]
    },
    {   
        country: "South Sudan",
        answers: ["Juba"]
    },
    {   
        country: "El Salvador",
        answers: ["San Salvador"]
    },
    {   
        country: "Syrian Arab Republic",
        answers: ["Damascus"]
    },
    {   
        country: "Swaziland",
        answers: ["Mbabane"]
    },
    {   
        country: "Turks and Caicos Islands",
        answers: ["Cockburn Town"]
    },
    {   
        country: "Chad",
        answers: ["N'Djamena"]
    },
    {   
        country: "Togo",
        answers: ["Kara"]
    },
    {   
        country: "Thailand",
        answers: ["Bangkok"]
    },
    {   
        country: "Tajikistan",
        answers: ["Dushanbe"]
    },
    {   
        country: "Timor-Leste",
        answers: ["Dili"]
    },
    {   
        country: "Turkmenistan",
        answers: ["Ashgabat"]
    },
    {   
        country: "Tunisia",
        answers: ["Tunis"]
    },
    {   
        country: "Tonga",
        answers: ["Kolovai"]
    },
    {   
        country: "Turkey",
        answers: ["Ankara"]
    },
    {   
        country: "Trinidad and Tobago",
        answers: ["Port of Spain"]
    },
    {   
        country: "Tuvalu",
        answers: ["Funafuti"]
    },
    {   
        country: "Taiwan",
        answers: ["Taipei"]
    },
    {   
        country: "Tanzania",
        answers: ["Dodoma"]
    },
    {   
        country: "Ukraine",
        answers: ["Kiev"]
    },
    {   
        country: "Uganda",
        answers: ["Kampala"]
    },
    {   
        country: "United States",
        answers: ["Washington D.C."]
    },
    {   
        country: "Uruguay",
        answers: ["Montevideo"]
    },
    {   
        country: "Uzbekistan",
        answers: ["Tashkent"]
    },
    {   
        country: "Holy See (Vatican City State)",
        answers: ["Vatican City"]
    },
    {   
        country: "Saint Vincent and the Grenadines",
        answers: ["Kingstown"]
    },
    {   
        country: "Venezuela",
        answers: ["Caracas"]
    },
    {   
        country: "Vietnam",
        answers: ["Hanoi"]
    },
    {   
        country: "Vanuatu",
        answers: ["Port Vila"]
    },
    {   
        country: "Wallis and Futuna",
        answers: ["Mata-Utu"]
    },
    {   
        country: "Samoa",
        answers: ["Apia"]
    },
    {   
        country: "Yemen",
        answers: ["Al Hudaydah"]
    },
    {   
        country: "South Africa",
        answers: ["Pretoria"]
    },
    {   
        country: "Zambia",
        answers: ["Lusaka"]
    },
    {   
        country: "Zimbabwe",
        answers: ["Harare"]
    }
];

export { answersPool };