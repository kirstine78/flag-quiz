import COUNTRY_FLAGS from '@salesforce/resourceUrl/countryflags';

/**
 * Pool of multiple question objects in it.
 * For each game we will randomly select only NUMBER_OF_QUESTIONS_PER_GAME questions from this pool.
 * correctAnswer is a String.
 * wrongAnswers is an array of Strings; is meant to eventually hold 3 Cities.
 * shuffledAnswers is array of Strings; is meant to eventually hold 4 cities shuffled, 1 correct and 3 wrong.
 */
const questionsPool = [
    {
        country: "Andorra",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Andorra la Vella"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ad.png'
    },
    {
        country: "United Arab Emirates",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Abu Dhabi"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ae.png'
    },
    {
        country: "Afghanistan",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Kabul"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/af.png'
    },
    {
        country: "Antigua and Barbuda",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["St. John's"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ag.png'
    },
    {
        country: "Anguilla",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["The Valley"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ai.png'
    },
    {
        country: "Albania",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Tirana"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/al.png'
    },
    {
        country: "Armenia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Yerevan"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/am.png'
    },
    {
        country: "Angola",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Luanda"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ao.png'
    },
    {
        country: "Argentina",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Buenos Aires"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ar.png'
    },
    {
        country: "American Samoa",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Pago Pago"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/as.png'
    },
    {
        country: "Austria",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Vienna"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/at.png'
    },
    {
        country: "Australia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Canberra", "Melbourne", "Sydney", "Brisbane"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/au.png'
    },
    {
        country: "Aruba",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Oranjestad"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/aw.png'
    },
    {
        country: "Azerbaijan",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Baku"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/az.png'
    },
    {
        country: "Bosnia and Herzegovina",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Sarajevo"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ba.png'
    },
    {
        country: "Barbados",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Bridgetown"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/bb.png'
    },
    {
        country: "Bangladesh",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Dhaka"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/bd.png'
    },
    {
        country: "Belgium",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Brussels"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/be.png'
    },
    {
        country: "Burkina Faso",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Ouagadougou"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/bf.png'
    },
    {
        country: "Bulgaria",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Sofia"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/bg.png'
    },
    {
        country: "Bahrain",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Manama"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/bh.png'
    },
    {
        country: "Burundi",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Bujumbura"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/bi.png'
    },
    {
        country: "Benin",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Porto-Novo"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/bj.png'
    },
    {
        country: "Bermuda",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Hamilton"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/bm.png'
    },
    {
        country: "Brunei Darussalam",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Bandar Seri Begawan"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/bn.png'
    },
    {
        country: "Bolivia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Sucre"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/bo.png'
    },
    {
        country: "Brazil",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Rio de Janeiro"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/br.png'
    },
    {
        country: "Bahamas",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Nassau"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/bs.png'
    },
    {
        country: "Bhutan",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Thimphu"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/bt.png'
    },
    {
        country: "Botswana",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Gaborone"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/bw.png'
    },
    {
        country: "Belarus",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Minsk"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/by.png'
    },
    {
        country: "Belize",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Belmopan"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/bz.png'
    },
    {
        country: "Canada",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Ottawa"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ca.png'
    },
    {
        country: "The Democratic Republic of the Congo",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Kinshasa"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/cd.png'
    },
    {
        country: "Central African Republic",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Bangui"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/cf.png'
    },
    {
        country: "Congo",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Brazzaville"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/cg.png'
    },
    {
        country: "Switzerland",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Bern"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ch.png'
    },
    {
        country: "Ivory Coast",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Yamoussoukro"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ci.png'
    },
    {
        country: "Cook Islands",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Avarua"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ck.png'
    },
    {
        country: "Chile",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Santiago"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/cl.png'
    },
    {
        country: "Cameroon",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Douala"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/cm.png'
    },
    {
        country: "China",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Beijing"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/cn.png'
    },
    {
        country: "Colombia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Cartagena"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/co.png'
    },
    {
        country: "Costa Rica",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["San Isidro de El General"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/cr.png'
    },
    {
        country: "Cuba",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Havana"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/cu.png'
    },
    {
        country: "Cape Verde",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Praia"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/cv.png'
    },
    {
        country: "Christmas Island",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Flying Fish Cove"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/cx.png'
    },
    {
        country: "Cyprus",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Nicosia"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/cy.png'
    },
    {
        country: "Czech Republic",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Prague"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/cz.png'
    },
    {
        country: "Germany",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Berlin"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/de.png'
    },
    {
        country: "Djibouti",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Djibouti"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/dj.png'
    },
    {
        country: "Denmark",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Copenhagen", "Esbjerg", "Aarhus", "Odense"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/dk.png'
    },
    {
        country: "Dominica",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Roseau"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/dm.png'
    },
    {
        country: "Dominican Republic",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Santo Domingo"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/do.png'
    },
    {
        country: "Algeria",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Algiers"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/dz.png'
    },
    {
        country: "Ecuador",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Quito"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ec.png'
    },
    {
        country: "Estonia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Tallinn"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ee.png'
    },
    {
        country: "Egypt",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Cairo"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/eg.png'
    },
    {
        country: "Western Sahara",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Laayoune"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/eh.png'
    },
    {
        country: "Eritrea",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Asmara"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/er.png'
    },
    {
        country: "Spain",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Madrid"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/es.png'
    },
    {
        country: "Ethiopia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Addis Ababa"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/et.png'
    },
    {
        country: "Finland",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Helsinki"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/fi.png'
    },
    {
        country: "Fiji",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Suva"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/fj.png'
    },
    {
        country: "Falkland Islands (Malvinas)",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Stanley"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/fk.png'
    },
    {
        country: "Faroe Islands",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Thorshavn"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/fo.png'
    },
    {
        country: "France",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Paris"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/fr.png'
    },
    {
        country: "Gabon",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Libreville"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ga.png'
    },
    {
        country: "United Kingdom",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["London"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/gb.png'
    },
    {
        country: "Grenada",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["St. George's"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/gd.png'
    },
    {
        country: "Georgia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Tbilisi"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ge.png'
    },
    {
        country: "French Guiana",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Cayenne"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/gf.png'
    },
    {
        country: "Guernsey",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["St. Peter Port"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/gg.png'
    },
    {
        country: "Ghana",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Accra"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/gh.png'
    },
    {
        country: "Gibraltar",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Gibraltar"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/gi.png'
    },
    {
        country: "Greenland",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Nuuk"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/gl.png'
    },
    {
        country: "Gambia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Banjul"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/gm.png'
    },
    {
        country: "Guinea",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Conakry"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/gn.png'
    },
    {
        country: "Equatorial Guinea",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Malabo"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/gq.png'
    },
    {
        country: "Greece",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Athens"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/gr.png'
    },
    {
        country: "South Georgia and the South Sandwich Islands",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Grytviken"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/gs.png'
    },
    {
        country: "Guatemala",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Guatemala City"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/gt.png'
    },
    {
        country: "Guam",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Dededo"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/gu.png'
    },
    {
        country: "Guinea-Bissau",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Bissau"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/gw.png'
    },
    {
        country: "Guyana",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Georgetown"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/gy.png'
    },
    {
        country: "Honduras",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Tegucigalpa"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/hn.png'
    },
    {
        country: "Croatia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Zagreb"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/hr.png'
    },
    {
        country: "Haiti",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Port-au-Prince"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ht.png'
    },
    {
        country: "Hungary",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Budapest"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/hu.png'
    },
    {
        country: "Indonesia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Jakarta"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/id.png'
    },
    {
        country: "Ireland",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Dublin"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ie.png'
    },
    {
        country: "Israel",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Jerusalem"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/il.png'
    },
    {
        country: "Isle of Man",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Douglas"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/im.png'
    },
    {
        country: "India",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["New Delhi", "Mumbai", "Bangalore", "Kolkata", "Jaipur"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/in.png'
    },
    {
        country: "Iraq",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Baghdad"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/iq.png'
    },
    {
        country: "Iran",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Tehran"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ir.png'
    },
    {
        country: "Iceland",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Reykjavik"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/is.png'
    },
    {
        country: "Italy",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Rome"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/it.png'
    },
    {
        country: "Jersey",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["St. Helier"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/je.png'
    },
    {
        country: "Jamaica",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Kingston"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/jm.png'
    },
    {
        country: "Jordan",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Amman"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/jo.png'
    },
    {
        country: "Japan",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Tokyo"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/jp.png'
    },
    {
        country: "Kenya",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Nairobi"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ke.png'
    },
    {
        country: "Kyrgyzstan",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Bishkek"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/kg.png'
    },
    {
        country: "Cambodia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Phnom Penh"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/kh.png'
    },
    {
        country: "Kiribati",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Tarawa"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ki.png'
    },
    {
        country: "Comoros",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Moroni"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/km.png'
    },
    {
        country: "Saint Kitts and Nevis",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Basseterre"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/kn.png'
    },
    {
        country: "Democratic People's Republic of Korea",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Pyongyang"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/kp.png'
    },
    {
        country: "Republic of Korea",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Seoul"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/kr.png'
    },
    {
        country: "Kuwait",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Kuwait City"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/kw.png'
    },
    {
        country: "Cayman Islands",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["George Town"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ky.png'
    },
    {
        country: "Kazakhstan",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Astana"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/kz.png'
    },
    {
        country: "Lao People's Democratic Republic",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Vientiane"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/la.png'
    },
    {
        country: "Lebanon",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Beirut"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/lb.png'
    },
    {
        country: "Saint Lucia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Castries"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/lc.png'
    },
    {
        country: "Liechtenstein",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Vaduz"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/li.png'
    },
    {
        country: "Sri Lanka",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Sri Jayawardenapura Kotte", "Moratuwa", "Negombo", "Trincomalee", "Dambulla"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/lk.png'
    },
    {
        country: "Liberia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Monrovia"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/lr.png'
    },
    {
        country: "Lesotho",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Maseru"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ls.png'
    },
    {
        country: "Lithuania",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Vilnius"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/lt.png'
    },
    {
        country: "Luxembourg",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Luxembourg"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/lu.png'
    },
    {
        country: "Latvia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Riga"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/lv.png'
    },
    {
        country: "Libya",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Tripoli"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ly.png'
    },
    {
        country: "Morocco",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Rabat"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ma.png'
    },
    {
        country: "Monaco",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Monaco"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/mc.png'
    },
    {
        country: "Moldova",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Chisinau"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/md.png'
    },
    {
        country: "Montenegro",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Podgorica"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/me.png'
    },
    {
        country: "Madagascar",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Antananarivo"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/mg.png'
    },
    {
        country: "Marshall Islands",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Majuro"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/mh.png'
    },
    {
        country: "Macedonia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Skopje"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/mk.png'
    },
    {
        country: "Mali",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Bamako"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ml.png'
    },
    {
        country: "Myanmar",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Naypyidaw"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/mm.png'
    },
    {
        country: "Mongolia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Ulaanbaatar"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/mn.png'
    },
    {
        country: "Northern Mariana Islands",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Saipan"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/mp.png'
    },
    {
        country: "Mauritania",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Nouakchott"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/mr.png'
    },
    {
        country: "Montserrat",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Plymouth"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ms.png'
    },
    {
        country: "Malta",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Valletta"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/mt.png'
    },
    {
        country: "Mauritius",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Port Louis"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/mu.png'
    },
    {
        country: "Maldives",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Hinnavaru"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/mv.png'
    },
    {
        country: "Malawi",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Lilongwe"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/mw.png'
    },
    {
        country: "Mexico",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Mexico City"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/mx.png'
    },
    {
        country: "Malaysia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Kuala Lumpur"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/my.png'
    },
    {
        country: "Mozambique",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Maputo"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/mz.png'
    },
    {
        country: "Namibia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Windhoek"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/na.png'
    },
    {
        country: "New Caledonia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Le Mont-Dore"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/nc.png'
    },
    {
        country: "Niger",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Niamey"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ne.png'
    },
    {
        country: "Norfolk Island",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Kingston"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/nf.png'
    },
    {
        country: "Nigeria",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Abuja"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ng.png'
    },
    {
        country: "Nicaragua",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Managua"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ni.png'
    },
    {
        country: "Netherlands",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Amsterdam"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/nl.png'
    },
    {
        country: "Norway",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Oslo"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/no.png'
    },
    {
        country: "Nepal",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Kathmandu"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/np.png'
    },
    {
        country: "Nauru",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Yaren"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/nr.png'
    },
    {
        country: "Niue",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Alofi"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/nu.png'
    },
    {
        country: "New Zealand",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Wellington"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/nz.png'
    },
    {
        country: "Oman",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Muscat"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/om.png'
    },
    {
        country: "Panama",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Panama City"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/pa.png'
    },
    {
        country: "Peru",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Lima"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/pe.png'
    },
    {
        country: "French Polynesia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Papeete"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/pf.png'
    },
    {
        country: "Papua New Guinea",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Port Moresby"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/pg.png'
    },
    {
        country: "Philippines",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Manila"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ph.png'
    },
    {
        country: "Pakistan",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Islamabad"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/pk.png'
    },
    {
        country: "Poland",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Warsaw"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/pl.png'
    },
    {
        country: "Saint Pierre and Miquelon",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["St. Pierre"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/pm.png'
    },
    {
        country: "Puerto Rico",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["San Juan"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/pr.png'
    },
    {
        country: "State of Palestine",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Jerusalem"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ps.png'
    },
    {
        country: "Portugal",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Lisbon"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/pt.png'
    },
    {
        country: "Palau",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Ngerulmud"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/pw.png'
    },
    {
        country: "Paraguay",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Ciudad del Este"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/py.png'
    },
    {
        country: "Qatar",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Doha"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/qa.png'
    },
    {
        country: "Romania",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Bucharest"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ro.png'
    },
    {
        country: "Serbia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Belgrade"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/rs.png'
    },
    {
        country: "Russian Federation",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Moscow"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ru.png'
    },
    {
        country: "Rwanda",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Kigali"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/rw.png'
    },
    {
        country: "Saudi Arabia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Riyadh"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/sa.png'
    },
    {
        country: "Solomon Islands",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Honiara"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/sb.png'
    },
    {
        country: "Seychelles",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Victoria"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/sc.png'
    },
    {
        country: "Sudan",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Khartoum"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/sd.png'
    },
    {
        country: "Sweden",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Stockholm"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/se.png'
    },
    {
        country: "Singapore",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Singapore"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/sg.png'
    },
    {
        country: "Slovenia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Ljubljana"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/si.png'
    },
    {
        country: "Slovakia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Bratislava"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/sk.png'
    },
    {
        country: "Sierra Leone",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Freetown"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/sl.png'
    },
    {
        country: "San Marino",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["San Marino"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/sm.png'
    },
    {
        country: "Senegal",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Dakar"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/sn.png'
    },
    {
        country: "Somalia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Mogadishu"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/so.png'
    },
    {
        country: "Suriname",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Paramaribo"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/sr.png'
    },
    {
        country: "South Sudan",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Juba"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ss.png'
    },
    {
        country: "El Salvador",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["San Salvador"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/sv.png'
    },
    {
        country: "Syrian Arab Republic",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Damascus"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/sy.png'
    },
    {
        country: "Swaziland",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Mbabane"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/sz.png'
    },
    {
        country: "Turks and Caicos Islands",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Cockburn Town"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/tc.png'
    },
    {
        country: "Chad",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["N'Djamena"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/td.png'
    },
    {
        country: "Togo",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Kara"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/tg.png'
    },
    {
        country: "Thailand",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Bangkok"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/th.png'
    },
    {
        country: "Tajikistan",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Dushanbe"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/tj.png'
    },
    {
        country: "Timor-Leste",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Dili"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/tl.png'
    },
    {
        country: "Turkmenistan",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Ashgabat"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/tm.png'
    },
    {
        country: "Tunisia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Tunis"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/tn.png'
    },
    {
        country: "Tonga",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Kolovai"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/to.png'
    },
    {
        country: "Turkey",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Ankara"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/tr.png'
    },
    {
        country: "Trinidad and Tobago",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Port of Spain"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/tt.png'
    },
    {
        country: "Tuvalu",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Funafuti"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/tv.png'
    },
    {
        country: "Taiwan",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Taipei"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/tw.png'
    },
    {
        country: "Tanzania",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Dodoma"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/tz.png'
    },
    {
        country: "Ukraine",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Kiev"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ua.png'
    },
    {
        country: "Uganda",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Kampala"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ug.png'
    },
    {
        country: "United States",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Washington D.C."],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/us.png'
    },
    {
        country: "Uruguay",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Montevideo"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/uy.png'
    },
    {
        country: "Uzbekistan",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Tashkent"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/uz.png'
    },
    {
        country: "Holy See (Vatican City State)",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Vatican City"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/va.png'
    },
    {
        country: "Saint Vincent and the Grenadines",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Kingstown"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/vc.png'
    },
    {
        country: "Venezuela",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Caracas"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ve.png'
    },
    {
        country: "Vietnam",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Hanoi"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/vn.png'
    },
    {
        country: "Vanuatu",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Port Vila"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/vu.png'
    },
    {
        country: "Wallis and Futuna",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Mata-Utu"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/wf.png'
    },
    {
        country: "Samoa",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Apia"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ws.png'
    },
    {
        country: "Yemen",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Al Hudaydah"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/ye.png'
    },
    {
        country: "South Africa",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Pretoria"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/za.png'
    },
    {
        country: "Zambia",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Lusaka"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/zm.png'
    },
    {
        country: "Zimbabwe",
        correctAnswer: null,
        wrongAnswers: null,
        shuffledAnswers: null,
        cityAnswerOptions: ["Harare"],
        imageUrl: COUNTRY_FLAGS + '/countryFlags/zw.png'
    }
];

export { questionsPool };
