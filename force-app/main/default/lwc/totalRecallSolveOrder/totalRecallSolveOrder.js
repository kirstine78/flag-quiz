/**
 * using drag and drop code found here:
 * https://lovesalesforceyes.blogspot.com/2020/12/sorting-table-rows-by-drag-and-drop-in.html
 */
import { LightningElement, api, track } from 'lwc';

export default class TotalRecallSolveOrder extends LightningElement {
    @api
    numberOfItems;
    @api
    itemsList;

    @api 
    numberOfItemsPerGame;  // i don't think this is used

    itemImgURLsList = [];  // list of strings; the imgUrls
    
    itemData = [];

    // we want to call on load
    connectedCallback() {
        // console.log('connectedCallback');
        // console.log('this.itemsList:', this.itemsList);
        this.shuffleItemImgURLsList();
        
        if (!this.itemImgURLsList) {
            // console.log('went in my if');
            this.itemImgURLsList = [...this.itemData]
        }
    }
    
    shuffleItemImgURLsList() {
        // console.log('this.numberOfItems:', this.numberOfItems);
        let randomUniqueNumbersSet = this.getSetOfRandomNumbers(this.numberOfItems, this.itemsList.length - 1);
        for (let i of randomUniqueNumbersSet) {
            // console.log(i);
            
            // hack, first convert to JSON string, then convert back to JavaScript object
            // this.itemsListShuffled.push(JSON.parse(JSON.stringify(this.itemsList[i])));

            // also add URLs to list
            this.itemImgURLsList.push(this.itemsList[i].imageUrl);
        }
    }

    /**
     * Get a set of unique numbers where length of set will be sizeOfSet.
     * Numbers can range from 0 -> max, both included
     */
    getSetOfRandomNumbers(sizeOfSet, max) {
        // console.log('getSetOfRandomNumbers');
        // console.log('sizeOfSet', sizeOfSet);
        // console.log('max', max);
        const numberSet = new Set()
        while (numberSet.size < sizeOfSet) {
            numberSet.add(Math.floor(Math.random() * (max + 1)));            
        }
        // console.log('numberSet', numberSet);
        return numberSet;
    }

    submitHandler() {
        // console.log('submitHandler');
        // in the totalRecall we will catch the event
        this.dispatchEvent(new CustomEvent('submitorder', {
            detail: this.itemImgURLsList
        }));
    }

    change(event) {
        // console.log('change');
        this.itemData = event.detail.join(', ');
        // console.log('this.itemData:', this.itemData);
    }

    dragStart(event) {
        // console.log('dragStart');
        event.target.classList.add('drag');
    }

    dragOver(event) {
        // console.log('dragOver');
        event.preventDefault();
        return false;
    }

    drop(event) {
        // console.log('drop');
        event.stopPropagation();
        const Element = this.template.querySelectorAll('.gameItems');
        // console.log('Element:', Element);
        // const DragValName = this.template.querySelector('.drag img').textContent;
        let DragValName = this.template.querySelector('.drag').src;
        // console.log('DragValName:', DragValName);
        let findSection = '/resource/';
        // let lengthOfFindSection  = findSection.length;
        let indexFindSectionOccurrenceDragValName = DragValName.search(findSection);

        DragValName = DragValName.substring(indexFindSectionOccurrenceDragValName);
        // console.log('DragValName substring:', DragValName);

        // const DropValName = event.target.textContent;
        let DropValName = event.target.src;
        // console.log('DropValName:', DropValName);
        let indexFindSectionOccurrenceDropValName = DropValName.search(findSection);
        DropValName = DropValName.substring(indexFindSectionOccurrenceDropValName);
        // console.log('DropValName substring:', DropValName);

        if (DragValName === DropValName) { 
            return false;
        }
        
        // find index in itemImgURLsList where the drop val name matches the name in the object
        const index = this.itemImgURLsList.indexOf(DropValName);
        const dragIndex = this.itemImgURLsList.indexOf(DragValName);

        // console.log('index:', index);
        // console.log('dragIndex:', dragIndex);
        this.itemImgURLsList = this.itemImgURLsList.reduce((acc, curVal, CurIndex) => {
            // console.log('CurIndex:', CurIndex);
            // console.log('curVal:', curVal);
            if (CurIndex === index) {
                // console.log('if');
                // return [...acc, DragValName, curVal];
                if(dragIndex > index){
                    return [...acc, DragValName, curVal];
                }else{
                    return [...acc, curVal, DragValName];
                }
            } else if (curVal !== DragValName) {
                // console.log('else if');
                return [...acc, curVal];
            }
            // console.log('acc:', acc);
            return acc;
        }, []);

        
        Array.from(Element).forEach(element => {
            element.classList.remove('drag');
        });

        this.itemImgURLsList.forEach(element => {
            // console.log(element);
        })
        return this.itemImgURLsList;
    }

    // Change(event) {
    //     console.log('Change');
    //     this.Data = event.detail.join(', ');
    //     console.log('this.Data:', this.Data);
    // }

    //  DragStart(event) {
    //     console.log('DragStart');
    //     event.target.classList.add('drag');
    // }

    // DragOver(event) {
    //     console.log('DragOver');
    //     event.preventDefault();
    //     return false;
    // }

    // Drop(event) {
    //     console.log('Drop');
    //     event.stopPropagation();
    //     const Element = this.template.querySelectorAll('.Items');
    //     const DragValName = this.template.querySelector('.drag').textContent;
    //     console.log('DragValName:', DragValName);
    //     const DropValName = event.target.textContent;

    //     if (DragValName === DropValName) { 
    //         return false;
    //     }
    //     const index = this.ElementList.indexOf(DropValName);
    //     console.log('index:', index);
    //     this.ElementList = this.ElementList.reduce((acc, curVal, CurIndex) => {
    //         if (CurIndex === index) {
    //             return [...acc, DragValName, curVal];
    //         } else if (curVal !== DragValName) {
    //             return [...acc, curVal];
    //         }
    //         return acc;
    //     }, []);

    //     Element.forEach(element => {
    //         element.classList.remove('drag');
    //     })
    //     return this.ElementList;
    // }

    
}